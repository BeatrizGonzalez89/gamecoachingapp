

## GameCoachingApp

Esta aplicación ha sido desarrollada por Beatriz González,
las herramientas que se utilizaron son las siguientes:

1. Java, Android nativo.
2. XML (Diseño de layout).
3. Android Studio.
4. Gestor de base de datos SQLite.
5. JSON.



Descripción de aplicación: permite a los usuario obtener videojuegos a la vez con la funcionalidad de entrenamientos de una forma dinámica al usuario.
