package com.snatik.prueba.common;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;

import com.snatik.prueba.R;

public class Music {

	public static boolean OFF = false;
	public static MediaPlayer mpBackground;
	public static void playCorrent() {
		if (!OFF) {
			MediaPlayer mp = MediaPlayer.create(Shared.context, R.raw.correct_answer);
			mp.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					mp.reset();
					mp.release();
					mp = null;
				}

			});
			mp.start();
		}
	}

	public static void playBackgroundMusic() {
		// TODO
		if (!OFF) {
			mpBackground = MediaPlayer.create(Shared.context, R.raw.background_music);
			mpBackground.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mpBackground) {
					mpBackground.reset();
					mpBackground.release();
					mpBackground = null;
				}

			});
			mpBackground.start();
		}
	}

	public static void showStar() {
		if (!OFF) {
			MediaPlayer mp = MediaPlayer.create(Shared.context, R.raw.star);
			mp.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					mp.reset();
					mp.release();
					mp = null;
				}

			});
			mp.start();
		}
	}
}
