package com.snatik.prueba.events;

import com.snatik.prueba.events.engine.FlipDownCardsEvent;
import com.snatik.prueba.events.engine.GameWonEvent;
import com.snatik.prueba.events.engine.HidePairCardsEvent;
import com.snatik.prueba.events.ui.BackGameEvent;
import com.snatik.prueba.events.ui.DifficultySelectedEvent;
import com.snatik.prueba.events.ui.FlipCardEvent;
import com.snatik.prueba.events.ui.NextGameEvent;
import com.snatik.prueba.events.ui.ResetBackgroundEvent;
import com.snatik.prueba.events.ui.StartEvent;
import com.snatik.prueba.events.ui.ThemeSelectedEvent;


public interface EventObserver {

	void onEvent(FlipCardEvent event);

	void onEvent(DifficultySelectedEvent event);

	void onEvent(HidePairCardsEvent event);

	void onEvent(FlipDownCardsEvent event);

	void onEvent(StartEvent event);

	void onEvent(ThemeSelectedEvent event);

	void onEvent(GameWonEvent event);

	void onEvent(BackGameEvent event);

	void onEvent(NextGameEvent event);

	void onEvent(ResetBackgroundEvent event);

}
