package com.snatik.prueba.model;

public class GameState {

	public int remainedSeconds;
	public int achievedStars;
	public int achievedScore;
	public int passedSeconds;
}
