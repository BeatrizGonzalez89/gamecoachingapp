package com.snatik.prueba.fragments;

import android.support.v4.app.Fragment;

import com.snatik.prueba.events.EventObserver;
import com.snatik.prueba.events.engine.FlipDownCardsEvent;
import com.snatik.prueba.events.engine.GameWonEvent;
import com.snatik.prueba.events.engine.HidePairCardsEvent;
import com.snatik.prueba.events.ui.BackGameEvent;
import com.snatik.prueba.events.ui.FlipCardEvent;
import com.snatik.prueba.events.ui.NextGameEvent;
import com.snatik.prueba.events.ui.ResetBackgroundEvent;
import com.snatik.prueba.events.ui.ThemeSelectedEvent;
import com.snatik.prueba.events.ui.DifficultySelectedEvent;
import com.snatik.prueba.events.ui.StartEvent;

public class BaseFragment extends Fragment implements EventObserver {

	@Override
	public void onEvent(FlipCardEvent event) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onEvent(DifficultySelectedEvent event) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onEvent(HidePairCardsEvent event) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onEvent(FlipDownCardsEvent event) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onEvent(StartEvent event) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onEvent(ThemeSelectedEvent event) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onEvent(GameWonEvent event) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onEvent(BackGameEvent event) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onEvent(NextGameEvent event) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onEvent(ResetBackgroundEvent event) {
		throw new UnsupportedOperationException();
	}

}
