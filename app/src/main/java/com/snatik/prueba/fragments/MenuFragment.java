package com.snatik.prueba.fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.VideoView;

import com.snatik.prueba.MainActivity;
import com.snatik.prueba.R;
import com.snatik.prueba.common.Music;
import com.snatik.prueba.common.Shared;
import com.snatik.prueba.engine.ScreenController;
import com.snatik.prueba.events.ui.BackGameEvent;
import com.snatik.prueba.events.ui.StartEvent;
import com.snatik.prueba.ui.PopupManager;
import com.snatik.prueba.utils.Utils;

public class MenuFragment extends Fragment {

	private ImageView mTitle;
	private ImageView mStartGameButton;
	private ImageView mStartButtonLights;
	private ImageView mTooltip;
	private ImageView mSettingsGameButton;
	private ImageView mGooglePlayGameButton;
	private ImageView mCloseButton;
    private VideoView mVideoPrueba;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.menu_fragment, container, false);
		mTitle = (ImageView) view.findViewById(R.id.title);
		mStartGameButton = (ImageView) view.findViewById(R.id.start_game_button);
		mSettingsGameButton = (ImageView) view.findViewById(R.id.settings_game_button);
        mVideoPrueba = (VideoView) view.findViewById(R.id.video_prueba);
		mVideoPrueba.setVisibility(View.INVISIBLE);
		mStartGameButton = (ImageView) view.findViewById(R.id.start_game_button);
		mCloseButton = (ImageView) view.findViewById(R.id.close_button);
		mCloseButton.setVisibility(View.INVISIBLE);

		mCloseButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onDestroyVideo();
			}
		});

		mSettingsGameButton.setSoundEffectsEnabled(false);
		mSettingsGameButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//PopupManager.showPopupSettings();
				Toast.makeText(getActivity(), "Esto es una prueba", Toast.LENGTH_LONG).show();
			}
		});
		mGooglePlayGameButton = (ImageView) view.findViewById(R.id.google_play_button);
		mTitle.setVisibility(View.INVISIBLE);
		mGooglePlayGameButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//Toast.makeText(getActivity(), "Esto es una prueba", Toast.LENGTH_LONG).show();
				ScreenController.getInstance().onBack();
				getActivity().finish();
			}
		});
		mStartButtonLights = (ImageView) view.findViewById(R.id.start_game_button_lights);
		mTooltip = (ImageView) view.findViewById(R.id.tooltip);
		mStartGameButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				mSettingsGameButton.setVisibility(View.INVISIBLE);
				mStartGameButton.setVisibility(View.INVISIBLE);
				mGooglePlayGameButton.setVisibility(View.INVISIBLE);
				mStartButtonLights.setVisibility(View.INVISIBLE);
				mTooltip.setVisibility(View.INVISIBLE);

				playVieo();

				// animate title from place and navigation buttons from place
				/*animateAllAssetsOff(new AnimatorListenerAdapter() {
					@Override
					public void onAnimationEnd(Animator animation) {
						//Shared.eventBus.notify(new StartEvent());
						Music.mpBackground.stop();
						playVieo();

						//Toast.makeText(getActivity(), "Play was pressed ", Toast.LENGTH_LONG).show();
					}
				});*/
			}
		});

		startLightsAnimation();
		startTootipAnimation();

		// play background music
		Music.playBackgroundMusic();
		return view;
	}
    public  void playVieo() {
		Music.mpBackground.stop();
		mVideoPrueba.setVisibility(View.VISIBLE);
		mCloseButton.setVisibility(View.VISIBLE);
        String path="android.resource://"+getActivity().getPackageName()+"/"+R.raw.video_prueba;
	    Uri uri= Uri.parse(path);
        mVideoPrueba.setVideoURI(uri);
		mVideoPrueba.requestFocus();
		mVideoPrueba.start();

    }
	public void onDestroyVideo() {
		ScreenController.getInstance().onBack();
		ScreenController.getInstance().openScreen(ScreenController.Screen.MENU);

	}
	protected void animateAllAssetsOff(AnimatorListenerAdapter adapter) {
		// title
		// 120dp + 50dp + buffer(30dp)
		ObjectAnimator titleAnimator = ObjectAnimator.ofFloat(mTitle, "translationY", Utils.px(-200));
		titleAnimator.setInterpolator(new AccelerateInterpolator(2));
		titleAnimator.setDuration(300);

		// lights
		ObjectAnimator lightsAnimatorX = ObjectAnimator.ofFloat(mStartButtonLights, "scaleX", 0f);
		ObjectAnimator lightsAnimatorY = ObjectAnimator.ofFloat(mStartButtonLights, "scaleY", 0f);

		// tooltip
		ObjectAnimator tooltipAnimator = ObjectAnimator.ofFloat(mTooltip, "alpha", 0f);
		tooltipAnimator.setDuration(100);

		// settings button
		ObjectAnimator settingsAnimator = ObjectAnimator.ofFloat(mSettingsGameButton, "translationY", Utils.px(120));
		settingsAnimator.setInterpolator(new AccelerateInterpolator(2));
		settingsAnimator.setDuration(300);

		// google play button
		ObjectAnimator googlePlayAnimator = ObjectAnimator.ofFloat(mGooglePlayGameButton, "translationY", Utils.px(120));
		googlePlayAnimator.setInterpolator(new AccelerateInterpolator(2));
		googlePlayAnimator.setDuration(300);

		// start button
		ObjectAnimator startButtonAnimator = ObjectAnimator.ofFloat(mStartGameButton, "translationY", Utils.px(130));
		startButtonAnimator.setInterpolator(new AccelerateInterpolator(2));
		startButtonAnimator.setDuration(300);

		AnimatorSet animatorSet = new AnimatorSet();
		animatorSet.playTogether(titleAnimator, lightsAnimatorX, lightsAnimatorY, tooltipAnimator, settingsAnimator, googlePlayAnimator, startButtonAnimator);
		animatorSet.addListener(adapter);
		animatorSet.start();
	}

	private void startTootipAnimation() {
		ObjectAnimator scaleY = ObjectAnimator.ofFloat(mTooltip, "scaleY", 0.8f);
		scaleY.setDuration(200);
		ObjectAnimator scaleYBack = ObjectAnimator.ofFloat(mTooltip, "scaleY", 1f);
		scaleYBack.setDuration(500);
		scaleYBack.setInterpolator(new BounceInterpolator());
		final AnimatorSet animatorSet = new AnimatorSet();
		animatorSet.setStartDelay(1000);
		animatorSet.playSequentially(scaleY, scaleYBack);
		animatorSet.addListener(new AnimatorListenerAdapter() {
			@Override
			public void onAnimationEnd(Animator animation) {
				animatorSet.setStartDelay(2000);
				animatorSet.start();
			}
		});
		mTooltip.setLayerType(View.LAYER_TYPE_HARDWARE, null);
		animatorSet.start();
	}

	private void startLightsAnimation() {
		ObjectAnimator animator = ObjectAnimator.ofFloat(mStartButtonLights, "rotation", 0f, 360f);
		animator.setInterpolator(new AccelerateDecelerateInterpolator());
		animator.setDuration(6000);
		animator.setRepeatCount(ValueAnimator.INFINITE);
		mStartButtonLights.setLayerType(View.LAYER_TYPE_HARDWARE, null);
		animator.start();
	}

}
